import EmberObject from '@ember/object';
import { computed } from '@ember/object';
import { A } from '@ember/array';

export default EmberObject.extend({
  contextual: false,
  checkboxValue: false,
  selectValue: null,
  dateValue: null,
  multiselectValue: null,
  optionValuePath: 'value',
  optionLabelPath: 'label',
  alternateLabel: null,
  searchPlaceHolder: null,
  columnSize: null,
  type: 'text',
  rawValue: false,
  hidden: false,

  init() {
    this._super(...arguments);
    this.set('multiselectValue', A([]));
  },

  displayName: computed('alternateLabel', 'searchPlaceHolder', function() {
    if (this.get('alternateLabel')) {
      return this.get('alternateLabel');
    }

    return this.get('searchPlaceHolder');
  }),

  loadFilter: function(obj){
    for (var key in obj) {
      this.set(key, obj[key]);
    }
  },

  getValueKey: function() {
    let type = this.get('type');

    if (type === 'text') {
      return 'searchVal';
    }

    return type + 'Value';
  },

  getValue: function() {
    return this.get(this.getValueKey());
  },

  setValue: function(value) {
    let type = this.get('type');
    let optionValuePath = this.get('optionValuePath');

    if (type === 'multiselect') {
      let options = this.get('content').mapBy(optionValuePath);

      value = value.map(o => {
        return this.get('content').objectAt(options.indexOf(o));
      });

      this.set(this.getValueKey(), value);
    } else if (type === 'select') {
      let match = null;

      this.get('content').forEach(o => {
        if (o[optionValuePath] === value) {
          match = o;
        }
      });

      this.set(this.getValueKey(), match);
    } else {
      this.set(this.getValueKey(), value);
    }
  },

  getReadableValue: function() {
    let value = null;
    let type = this.get('type');
    let valuePath = this.get('optionValuePath');

    if (type === 'text') {
      return this.get('searchVal');
    }

    value = this.get(type + 'Value');

    if (value === null || value === 'undefined') {
      return null;
    }

    if (Array.isArray(value) && valuePath && type === 'multiselect') {
      value = value.mapBy(valuePath);
    } else if (type === 'date') {
      value = new Date(value).toISOString();
    } else if (type === 'select' && valuePath && value[valuePath]) {
      value = value[valuePath];

      if (value === 'true') {
        value = true;
      } else if (value === 'false') {
        value = false;
      }
    }

    return value;
  }
});
