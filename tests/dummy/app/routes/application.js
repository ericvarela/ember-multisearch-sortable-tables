import Route from '@ember/routing/route';
import RSVP from 'rsvp';
import { A } from '@ember/array';
import NewItem from '../utils/new-item';
import faker from 'faker';

export default Route.extend({

  model(){

    var itemsArray = A ([]);

    for(let i=0; i< 100; i++){

      let item = NewItem.create();
      item.set("firstName",faker.name.firstName());
      item.set("lastName",faker.name.lastName());

      itemsArray.push(item);
      if(i % 4 === 0){
        item.set("disabled",true);

      }
    }

    return RSVP.hash({
      items:
        itemsArray

    });
    }
});
